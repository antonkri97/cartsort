package main

import "fmt"
import "errors"

// Suit - масть
type Suit int

const (
	clubs    Suit = 4
	diamonds Suit = 3
	hearts   Suit = 2
	spades   Suit = 1
)

// Cart - игральная карта
type Cart struct {
	rank int
	suit Suit
}

// Deck - колода
type Deck [52]Cart

func main() {
	d := initDeck()
	d.sort()
	d.print()
}

func initDeck() Deck {
	deck := Deck{}
	var c int

	for s := 0; s < 4; s++ {
		for r := 0; r < 13; r++ {
			deck[c] = Cart{r + 1, Suit(s + 1)}
			c++
		}
	}
	return deck
}

func (d *Deck) sort() {
	for i := 0; i < 52; i++ {
		for j := 0; j < 51; j++ {
			if !compare(d[i], d[j]) {
				d[i], d[j] = d[j], d[i]
			}
		}
	}
}

func compare(a, b Cart) bool {
	if a.suit > b.suit {
		return true
	}

	if a.suit == b.suit {
		return a.rank > b.rank
	}

	return false
}

func (d Deck) print() {
	var suit string

	for _, cart := range d {
		suit, _ = getSuitNameByInt(int(cart.suit))
		fmt.Printf("%s : %d\n", suit, cart.rank)
	}
	fmt.Println()
}

func getSuitNameByInt(n int) (string, error) {
	switch n {
	case 1:
		return "Clubs", nil
	case 2:
		return "Diamonds", nil
	case 3:
		return "Hearts", nil
	case 4:
		return "Spades", nil
	default:
		return "", errors.New("undefined")
	}
}
