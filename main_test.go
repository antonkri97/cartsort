package main

import (
	"testing"
)

type TestPair struct {
	unsorted Deck
	sorted   Deck
}

var tests = TestPair{
	Deck{
		Cart{8, Suit(2)},
		Cart{2, Suit(1)},
		Cart{3, Suit(3)},
		Cart{1, Suit(2)},
		Cart{6, Suit(3)},
		Cart{6, Suit(1)},
		Cart{7, Suit(1)},
		Cart{8, Suit(1)},
		Cart{9, Suit(1)},
		Cart{10, Suit(1)},
		Cart{11, Suit(1)},
		Cart{12, Suit(1)},
		Cart{13, Suit(2)},
		Cart{4, Suit(1)},
		Cart{2, Suit(2)},
		Cart{3, Suit(2)},
		Cart{4, Suit(2)},
		Cart{6, Suit(4)},
		Cart{6, Suit(2)},
		Cart{7, Suit(2)},
		Cart{1, Suit(1)},
		Cart{3, Suit(4)},
		Cart{10, Suit(2)},
		Cart{11, Suit(2)},
		Cart{12, Suit(2)},
		Cart{13, Suit(1)},
		Cart{1, Suit(3)},
		Cart{2, Suit(3)},
		Cart{3, Suit(1)},
		Cart{4, Suit(3)},
		Cart{5, Suit(3)},
		Cart{5, Suit(1)},
		Cart{7, Suit(3)},
		Cart{8, Suit(3)},
		Cart{9, Suit(3)},
		Cart{10, Suit(3)},
		Cart{11, Suit(3)},
		Cart{12, Suit(3)},
		Cart{13, Suit(3)},
		Cart{1, Suit(4)},
		Cart{2, Suit(4)},
		Cart{9, Suit(2)},
		Cart{7, Suit(4)},
		Cart{13, Suit(4)},
		Cart{5, Suit(4)},
		Cart{4, Suit(4)},
		Cart{8, Suit(4)},
		Cart{9, Suit(4)},
		Cart{10, Suit(4)},
		Cart{11, Suit(4)},
		Cart{12, Suit(4)},
		Cart{5, Suit(2)},
	},
	Deck{
		Cart{1, Suit(1)},
		Cart{2, Suit(1)},
		Cart{3, Suit(1)},
		Cart{4, Suit(1)},
		Cart{5, Suit(1)},
		Cart{6, Suit(1)},
		Cart{7, Suit(1)},
		Cart{8, Suit(1)},
		Cart{9, Suit(1)},
		Cart{10, Suit(1)},
		Cart{11, Suit(1)},
		Cart{12, Suit(1)},
		Cart{13, Suit(1)},
		Cart{1, Suit(2)},
		Cart{2, Suit(2)},
		Cart{3, Suit(2)},
		Cart{4, Suit(2)},
		Cart{5, Suit(2)},
		Cart{6, Suit(2)},
		Cart{7, Suit(2)},
		Cart{8, Suit(2)},
		Cart{9, Suit(2)},
		Cart{10, Suit(2)},
		Cart{11, Suit(2)},
		Cart{12, Suit(2)},
		Cart{13, Suit(2)},
		Cart{1, Suit(3)},
		Cart{2, Suit(3)},
		Cart{3, Suit(3)},
		Cart{4, Suit(3)},
		Cart{5, Suit(3)},
		Cart{6, Suit(3)},
		Cart{7, Suit(3)},
		Cart{8, Suit(3)},
		Cart{9, Suit(3)},
		Cart{10, Suit(3)},
		Cart{11, Suit(3)},
		Cart{12, Suit(3)},
		Cart{13, Suit(3)},
		Cart{1, Suit(4)},
		Cart{2, Suit(4)},
		Cart{3, Suit(4)},
		Cart{4, Suit(4)},
		Cart{5, Suit(4)},
		Cart{6, Suit(4)},
		Cart{7, Suit(4)},
		Cart{8, Suit(4)},
		Cart{9, Suit(4)},
		Cart{10, Suit(4)},
		Cart{11, Suit(4)},
		Cart{12, Suit(4)},
		Cart{13, Suit(4)},
	},
}

func TestCartSort(t *testing.T) {
	tests.unsorted.sort()

	for c, justSortedCart := range tests.unsorted {
		if (justSortedCart.rank != tests.sorted[c].rank) || (int(justSortedCart.suit) != int(tests.sorted[c].suit)) {
			actualSuit, _ := getSuitNameByInt(int(justSortedCart.suit))
			expectedSuit, _ := getSuitNameByInt(int(tests.sorted[c].suit))

			t.Errorf("For %s:%d expected %s:%d",
				actualSuit,
				justSortedCart.rank,
				expectedSuit,
				tests.sorted[c].rank,
			)
		}
	}
}
